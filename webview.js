const path = require('path');

module.exports = (Franz, options) => {
  const getMessages = () => {
    // get unread messages
    //const updates = document.getElementById('franz').getAttribute('data-unread');
    let updates = 0;
    if (document.querySelectorAll(".badge-circle").length === 2) {
      updates += 1;
    }
    if (document.querySelectorAll(".badge-bubble").length === 2) {
      updates += 1;
    }

    // get conversations in 'My Inbox'
    //const inbox = document.getElementById('franz').getAttribute('data-inbox');
    const inbox = 0;

    // set Franz badge
    // updates => active unread count
    // inbox => passive unread count
    Franz.setBadge(updates, inbox);
  };

  // check for new messages every second and update Franz badge
  Franz.loop(getMessages);
};
